import { HttpService } from '@nestjs/axios';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { firstValueFrom, map } from 'rxjs';
import { ArticlesService } from 'src/articles/services/articles.service';
import { CreateArticleDto } from 'src/articles/dto/articles.dto';

@Injectable()
export class ApiService implements OnModuleInit {
  constructor(
    private readonly httpService: HttpService,
    private readonly articleService: ArticlesService,
  ) {}

  async onModuleInit() {
    await this.handleApiData();
  }

  async getData(): Promise<object[]> {
    const response = this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(map((res) => res.data.hits));
    return firstValueFrom(response);
  }

  @Cron(CronExpression.EVERY_HOUR)
  async handleApiData(): Promise<void> {
    const articles: Array<object> = await this.getData();
    await Promise.all(
      articles.map((data: CreateArticleDto) => {
        return this.articleService.createArticle(data);
      }),
    );
  }
}
