import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ScheduleModule } from '@nestjs/schedule';
import { ArticlesModule } from 'src/articles/articles.module';
import { ApiService } from './services/api.service';

@Module({
  imports: [HttpModule, ArticlesModule, ScheduleModule.forRoot()],
  providers: [ApiService],
})
export class ApiModule {}
