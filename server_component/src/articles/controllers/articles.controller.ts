import { Controller, Get, Post, Body, Delete, Param } from '@nestjs/common';
import { ArticlesService } from '../services/articles.service';
import { CreateArticleDto } from '../dto/articles.dto';
import { Article } from '../schemas/article.schema';

@Controller('articles')
export class ArticlesController {
  constructor(private articlesService: ArticlesService) {}

  @Get()
  getAllArticles(): Promise<Article[]> {
    return this.articlesService.findArticles();
  }

  @Get(':id')
  getArticleById(@Param('id') id: string): Promise<Article> {
    return this.articlesService.findArticleById(id);
  }

  @Post()
  createArticle(@Body() createArticleDto: CreateArticleDto): Promise<Article> {
    return this.articlesService.createArticle(createArticleDto);
  }

  @Delete(':id')
  deleteArticle(@Param('id') id: string): Promise<Article> {
    return this.articlesService.deleteArticle(id);
  }
}
