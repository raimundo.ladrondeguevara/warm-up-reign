import { Article } from 'src/articles/schemas/article.schema';

export const articleStub = (): Article => {
  return {
    objectID: 'this is an objectID',
    created_at: 'this is a created_at',
    title: 'this is a title',
    url: 'this is a url',
    author: 'this is an author',
    story_title: 'this is a story_title',
    story_url: 'this is a story_url',
    status: true,
    deleted_at: new Date(),
  };
};
