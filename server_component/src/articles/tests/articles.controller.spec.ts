import { Test } from '@nestjs/testing';
import { ArticlesController } from '../controllers/articles.controller';
import { ArticlesService } from '../services/articles.service';

jest.mock('../__mocks__/articles.service');

describe('ArticlesController', () => {
  let articlesController: ArticlesController;
  let articlesService: ArticlesService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [],
      controllers: [ArticlesController],
      providers: [ArticlesService],
    }).compile();

    articlesController = moduleRef.get<ArticlesController>(ArticlesController);
    articlesService = moduleRef.get<ArticlesService>(ArticlesService);
    jest.clearAllMocks();
  });

  describe('getAllArticles', () => {
    describe('when getAllArticles is called', () => {
      beforeEach(async () => {
        await articlesController.getAllArticles();
      });

      test('then it should call articlesService', () => {
        expect(articlesService.findArticles).toBeCalledWith();
      });
    });
  });
});
