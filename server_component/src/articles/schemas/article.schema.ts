import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  objectID: string;

  @Prop()
  created_at: string;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop({ default: true })
  status: boolean;

  @Prop({ type: Date, default: null })
  deleted_at: Date;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
