import { articleStub } from '../tests/stubs/articles.stubs';

export const ArticlesService = jest.fn().mockReturnValue({
  findArticles: jest.fn().mockReturnValue(articleStub()),
  findArticleById: jest.fn().mockReturnValue(articleStub()),
  createArticle: jest.fn().mockReturnValue(articleStub()),
  deleteArticle: jest.fn().mockReturnValue(articleStub()),
});
