import { Article } from '../schemas/article.schema';
import { Injectable } from '@nestjs/common';
import { CreateArticleDto } from '../dto/articles.dto';
import { ArticlesRepository } from '../repository/articles.repository';

@Injectable()
export class ArticlesService {
  constructor(private readonly articleRepository: ArticlesRepository) {}

  findArticles(): Promise<Article[]> {
    return this.articleRepository.getAll();
  }

  findArticleById(id: string): Promise<Article> {
    return this.articleRepository.getOne({ id });
  }

  createArticle(createArticleDto: CreateArticleDto): Promise<Article> {
    return this.articleRepository.create(createArticleDto);
  }

  deleteArticle(id: string): Promise<Article> {
    return this.articleRepository.delete({ id });
  }
}
