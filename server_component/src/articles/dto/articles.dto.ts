import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class CreateArticleDto {
  @IsString()
  @IsNotEmpty()
  objectID: string;

  @IsString()
  @IsNotEmpty()
  created_at: string;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  url: string;

  @IsString()
  @IsNotEmpty()
  author: string;

  @IsString()
  story_title: string;

  @IsString()
  @IsNotEmpty()
  story_url: string;

  @IsBoolean()
  status: boolean;

  // @IsDate()
  // deleted_at: Date;
}
