import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { CreateArticleDto } from '../dto/articles.dto';
import { Article, ArticleDocument } from '../schemas/article.schema';

@Injectable()
export class ArticlesRepository {
  constructor(
    @InjectModel(Article.name)
    private articleModel: Model<ArticleDocument>,
  ) {}

  getAll(): Promise<Article[]> {
    return this.articleModel.find({ status: true, deleted_at: null }).exec();
  }

  getOne(filter: FilterQuery<Article>): Promise<Article> {
    return this.articleModel.findOne(filter).exec();
  }

  create(createArticleDto: CreateArticleDto): Promise<Article> {
    const newArticle = new this.articleModel(createArticleDto);
    return newArticle.save();
  }

  delete(filter: FilterQuery<Article>): Promise<Article> {
    return this.articleModel
      .findOneAndUpdate(
        filter,
        { status: false, deleted_at: new Date() },
        { new: true },
      )
      .exec();
  }
}
