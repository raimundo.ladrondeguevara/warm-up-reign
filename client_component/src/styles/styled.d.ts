// import original module declarations
import 'styled-components';

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
  row: {
      backgroundColor: string,
      border: string,
      hoverBackgroundColor: string,
      fontSize: string,
      
  },
  titleFont: {
    fontColor: string,
    fontSize: string,
},
  author: {
    fontColor: string,
},
  }
}
