import { DefaultTheme } from 'styled-components';

const myTheme: DefaultTheme = {
    row: {
        backgroundColor: '#ffffff',
        border: '1px #cccccc',
        hoverBackgroundColor: '#fafafa',
        fontSize: '13pt',
        
    },
    titleFont: {
        fontColor: '#333333',
        fontSize: '13pt',
    },
    author: {
        fontColor: '#999999',
    },
};

export { myTheme };