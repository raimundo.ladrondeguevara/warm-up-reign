import React from 'react';
import Article from './components/Article';
import ArticlesTable from './components/ArticlesTable';
import { Header } from './components/Header';

function App() {
  return (
    <React.Fragment>
      <Header />
      <Article />
      <ArticlesTable />
    </React.Fragment>
  );
}

export default App;
