import React from "react";
import styled from 'styled-components';

const StyledPrimaryTitle = styled.h1`
margin: 0;
font-size: 60pt;
font-family: "Comic Sans MS", "Comic Sans";
color: #fffeff;
`;

function PrimaryTitle(): JSX.Element {
    return (
        <StyledPrimaryTitle>HN News</StyledPrimaryTitle>
    );
}

export default PrimaryTitle;
