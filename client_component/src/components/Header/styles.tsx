import styled from "styled-components";

const StyledHeader = styled.div`
    background-color: #333333;
    padding: 50px;
`;

const StyledPrimaryTitle = styled.h1`
    margin: 0;
    font-size: 60pt;
    font-family: "Comic Sans MS", "Comic Sans";
    color: #fffeff;
`;

const StyledSecondaryTitle = styled.h2`
    margin: 0;
    font-size: 18pt;
    font-family: "Comic Sans MS", "Comic Sans";
    color: #fffeff;
    font-weight: lighter;
`;

export {StyledHeader, StyledPrimaryTitle, StyledSecondaryTitle}