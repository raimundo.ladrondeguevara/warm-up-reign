import styled from "styled-components";
import PrimaryTitle from "../PrimaryTitle";
import SecondaryTitle from "../SecondaryTitle";

const StyledHeader = styled.div`
    background-color: #333333;
    padding: 50px;
`;

function Header() {
    return (
        <StyledHeader>
            <PrimaryTitle />
            <SecondaryTitle />
        </StyledHeader>
    );
}

export default Header;