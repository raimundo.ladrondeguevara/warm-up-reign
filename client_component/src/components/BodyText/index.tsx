import React from 'react'
import styled from "styled-components";
// interface TypeText {
//     readonly type: string;
// }  

// const StyledBodyText = styled.p<TypeText>`
//     margin-bottom: 20px;
//     margin-top: 20px;
//     color: ${(props) => props.type === 'author' ? props.theme.author.fontColor : props.theme.titleFont.fontColor};
//     padding-left: ${(props) => (props.type === 'author' ? '20px' : '0' )};
// `;

const StyledBodyText = styled.p`
    margin-bottom: 20px;
    margin-top: 20px;
`;

// function BodyText({ children, type }: { children: React.ReactNode; type: string }) {
//     return (
//         <StyledBodyText type={type}>{children}</StyledBodyText>
//     );
// }

function BodyText({ children } : { children: React.ReactNode }): JSX.Element {
    return (
        <StyledBodyText>{children}</StyledBodyText>
    );
}

export { BodyText };