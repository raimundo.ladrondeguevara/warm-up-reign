import styled from "styled-components";
import { BodyText } from "../BodyText";
import { DeleteButton } from "../DeleteButton";

const StyledArticle = styled.div`
    display: flex;
    justify-content: space-between;
    width: 20%;
    margin: 20px 0;
`;

export type ArticleType = {
    readonly objectID: string;
    readonly story_title: string;
    readonly title: string;
    readonly author: string;
    readonly created_at: string;
    readonly story_url: string;
    readonly story_id: string;
    readonly url: string;
    readonly deleted_at: Date;
}

export async function getAllArticles() {
    const res = await fetch('http://localhost:3001/articles');
    const data: ArticleType[] = await res.json();
    return data;
};

export async function deleteArticle(objectID: string) {
    const res = await fetch(`${'http://localhost:3001/articles'}/${objectID}`, {
        method: 'deleteArticle',
    });
    return res.status;
};

function ArticleRow() {
    return (
        <StyledArticle>
            <BodyText children={undefined}></BodyText>
            {/* <BodyText>Author</BodyText> */}
            {/* <BodyText type="createdAt">{news.created_at}</BodyText>
            <DeleteButton /> */}
        </StyledArticle>
    );
}

export default ArticleRow;