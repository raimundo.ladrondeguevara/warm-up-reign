import { useEffect, useState } from "react";
import styled from "styled-components";
import Article, { ArticleType, getAllArticles } from "../Article";

const StyledArticlesTable = styled.div`
    width: 98%;
    margin: 0 auto;
    margin-top: 20px;
`;

function ArticlesTable() {

  const [articleList, setArticlelist] = useState<ArticleType[]>([])

  const displayableArticles = (article: ArticleType) => {
    if (article.deleted_at === null && (article.story_title || article.title) && (article.story_url || article.url)) {
        return true;
    } else {
        return false;
    }
  };

  useEffect(() => {
    const fetchApi = async () => {
      setArticlelist((await getAllArticles()).filter(displayableArticles))
    };
    fetchApi();
  })


  return (
    <StyledArticlesTable>
        {articleList.map((article: ArticleType) => (
          <div key={article.objectID}>
                      <a href={article.url || article.story_url} target='_blank' rel="noopener noreferrer">
                        <Article key={article.objectID}/>
                      </a>
          </div>
        ))}
    </StyledArticlesTable>
  );
}

export default ArticlesTable;