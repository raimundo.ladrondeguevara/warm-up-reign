import React from "react";
import styled from "styled-components";

const StyledSecondaryTitle = styled.h2`
    margin: 0;
    font-size: 18pt;
    font-family: "Comic Sans MS", "Comic Sans";
    color: #fffeff;
    font-weight: lighter;
`;

function SecondaryTitle(): JSX.Element {
    return (
        <StyledSecondaryTitle>We love hacker news</StyledSecondaryTitle>
    );
}

export default SecondaryTitle;